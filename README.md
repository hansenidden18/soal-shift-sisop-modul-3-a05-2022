# soal-shift-sisop-modul-3-A05-2022

## Nomor 1

### Deskripsi SOal
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

#### 1.A
Download dua file zip dan unzip file zip secara bersamaan menggunakan thread. Maka pertama akan membuat fungsi dalam thread untuk unzip file
```
    char *argument = (char *) args;
    char folder[100] = "/home/awan/modul/";
    char zippo[100] = "/home/awan/modul/";
    
    strcat(folder,argument);
    strcat(zippo,argument);

    if ((child = fork()) == 0) {
        execlp("unzip", "unzip", "-q", strcat(zippo,".zip"), "-d", folder, NULL);
    }
    else while ((wait(&status)) > 0);
```
Lalu dibuat thread join tuntuk menjalankan fungsi tersebut

```
    while(i < 2){
        int err = pthread_create(&(thread[i]), NULL, &unzip, (void*) name[i]);
        if(err != 0){
            printf("\n Can't create thread : [%s]", strerror(err));
        }
        else{
            printf("\n Create thread success\n");
        }
        i++;
    }
```

#### 1.B & 1.C
Decode secara bersamaan isi file quote dan file music. Maka pertama dibuat fungsi decode base64.

```
    static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}
```
Setelah itu buat fungsi untuk mendecode semua file yang ada agar bisa di thread. FUngsi ini digunakan dengan cara mengloop tiap file yang ada dan mendecode satu - satu setelah itu semua hasilnya akan ditulis di txt sesuai kategori dan akan disimpan dalam sebuah folder baru, yaitu folder hasil.

```
    FILE *fptr;
    FILE *writeFile;
    char *arg = (char *) args;
    char folder[100] = "/home/awan/modul/";
    char new[100] = "/home/awan/modul/";
    char hasil[100] = "/home/awan/modul/hasil/";

    strcat(hasil, arg);
    strcat(hasil, ".txt");
    strcat(folder, arg);
    strcat(new, arg);
    strcat(new, "/");
    strcat(new, arg);
    strcat(new, ".txt");
    
    struct dirent *dir;
    DIR *file = opendir(folder);
    writeFile = fopen(new, "w");
    fclose(writeFile);

    while((dir = readdir(file)) != NULL){
        if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
            char code[100];
            char answer[100];
            char Files[100] = "/home/awan/modul/";
            strcat(Files, arg);
            strcat(Files, "/");
            strcat(Files, dir->d_name);
            
            fptr = fopen(Files, "r");
            fscanf(fptr, "%s", code);
            long decode_size = strlen(code);
            char *decoded_data = base64_decode(code, decode_size, &decode_size);
            fclose(fptr);

            writeFile = fopen(new, "a");
            fprintf(writeFile, "%s\n", decoded_data);
            fclose(writeFile);
            
        }
    }
    closedir(file);

    if ((child = fork()) == 0) {
        execlp("mkdir", "mkdir", "-p", "/home/awan/modul/hasil", NULL);
    }
    else while ((wait(&status)) > 0);

    if ((child = fork()) == 0) {
        execlp("mv", "mv", "-f", new, hasil, NULL);
    }
    else while ((wait(&status)) > 0);
```

#### 1.D
Zip folder hasil menjadi hasil.zip dan diberi password 'mihinomenest[Nama user]'.

```
if ((child = fork()) == 0) {
        execlp("zip", "zip", "-r", "hasil.zip", "hasil", "--password", "mihinomenestawan", NULL);
    }
    else while ((wait(&status)) > 0);
```

Menggunakan zip sederhana untuk mengzip dan memberi password

#### 1.E
Unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Maka pertama membuat 2 fungsi void untuk dijalankan pada thread. 2 fungsi itu yaitu fungsi untuk membuat no.txt dan fungsi untuk mengunzip hasil.zip

```
void *small_unzip(void *args){
    if ((child = fork()) == 0) {
        execlp("unzip", "unzip", "-P", "mihinomenestawan", "hasil.zip", NULL);
    }
    else while ((wait(&status)) > 0);
}

void *create_no(void *args){
    FILE *writeFile;
    char new[100] = "/home/awan/modul/no.txt";
    char hasil[100] = "/home/awan/modul/hasil/";

    writeFile = fopen(new, "w");
    fclose(writeFile);
    writeFile = fopen(new, "a");
    fprintf(writeFile, "%s\n", "No");
    fclose(writeFile);

    if ((child = fork()) == 0) {
        execlp("mv", "mv", "-f", new, hasil, NULL);
    }
    else while ((wait(&status)) > 0);
}
```
Setelah itu kita joinkan thread masing masing
```
    int err1 = pthread_create(&(thread[0]), NULL, &small_unzip, NULL);
    if(err1 != 0){
        printf("\n Can't create thread : [%s]", strerror(err1));
    }
    else{
        printf("\n Create thread success\n");
    }

    int err2 = pthread_create(&(thread[1]), NULL, &create_no, NULL);
    if(err2 != 0){
        printf("\n Can't create thread : [%s]", strerror(err2));
    }
    else{
        printf("\n Create thread success\n");
    }
    pthread_join( thread[0], NULL);
    pthread_join( thread[1], NULL);
```
Lalu kita zip kembali menjadi 1 yaitu folder zip dengan password lama
```
    if ((child = fork()) == 0) {
        execlp("zip", "zip", "-r", "hasil.zip", "hasil", "--password", "mihinomenestawan", NULL);
    }
    else while ((wait(&status)) > 0);
```

## Nomor 3

### Deskripsi Soal

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

#### 3.A, 3.B, 3.C

Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

##### Pemanggilan fungsi listFilesRecursively

```
listFilesRecursively("/home/adindazhr/shift3/hartakarun");
```

##### Fungsi listFilesRecursively

```
void listFilesRecursively(char *basePath)
{
    char path_src[1000]; 
    struct dirent *dp; 
    struct dirent *lebihmasuk; 
    struct stat statbuf; 
    pthread_t t_id;
    DIR *dir = opendir(basePath); 
    char tempat[3000];
 
    if(!dir)
        return; 
 
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0) 
        {
 
            strcpy(path_src, basePath); 
            strcat(path_src, "/"); 
 
            
            if (dp->d_type == 4) //cek ini directory atau bukan
            {
            	rmdir(path_src);
            	/*DIR *dalemlagi;
            	char tempdalem[3000];
            	sprintf(tempdalem, "%s/%s", path_src, dp->d_name);
            	dalemlagi = opendir(tempdalem);
            	while(lebihmasuk = readdir(dalemlagi))
            	{
            		if(!strcmp(lebihmasuk->d_name,".") || !strcmp(lebihmasuk->d_name, ".."))
            		continue;
            		sprintf(tempat, "%s%s/%s", path_src, dp->d_name, lebihmasuk->d_name);
            		pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
            		pthread_join(t_id, NULL);
            		listFilesRecursively(path_src);
            	}*/
            }
            else {
            	sprintf(tempat, "%s%s", path_src, dp->d_name);
            	pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
            	pthread_join(t_id, NULL);
            	listFilesRecursively(path_src);
            }
            
        }
    }
    closedir(dir);
}
```

Pertama akan dilakukan pengecekan apakah path tersebut adalah directory atau bukan. Jika path tersebut adalah directory maka akan dihapus. Seharusnya, path tersebut akan dimasuki lebih dalam lagi dan diambil file file didalamnya. Namun karena terjadi segmentation fault maka codingan untuk hal tersebut saya jadikan comment.

```
if (dp->d_type == 4) //cek ini directory atau bukan
{
    rmdir(path_src);
    /*DIR *dalemlagi;
    char tempdalem[3000];
    sprintf(tempdalem, "%s/%s", path_src, dp->d_name);
    dalemlagi = opendir(tempdalem);
    while(lebihmasuk = readdir(dalemlagi))
    {
        if(!strcmp(lebihmasuk->d_name,".") || !strcmp(lebihmasuk->d_name, ".."))
        continue;
        sprintf(tempat, "%s%s/%s", path_src, dp->d_name, lebihmasuk->d_name);
        pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
        pthread_join(t_id, NULL);
        listFilesRecursively(path_src);
    }*/
}
```

Selanjutnya, jika path tersebut adalah file, maka akan dilanjutkan ke proses selanjutnya

```
else 
{
    sprintf(tempat, "%s%s", path_src, dp->d_name);
    pthread_create(&t_id, NULL, &auto_check, (void *)tempat);
    pthread_join(t_id, NULL);
    listFilesRecursively(path_src);
}
```

##### Fungsi auto_check

Fungsi ini akan mengambil basename dan extension dari file.

```
void *auto_check(void *args)
{
    char path_des[1000];
    char *path_src = (char*)malloc(sizeof(char));
    path_src = (char*)args;
    char *bname = basename(path_src);
    char *ext =strdup(get_filename_ext(bname));
    //printf("%s\n", bname);
    stringLwr(ext);
    if(strcmp(ext,"gz")== 0)
    {
        ext = "tar.gz";
    }
    strcpy(path_des, "/home/adindazhr/shift3/hartakarun");
    strcat(path_des, "/");
    strcat(path_des, ext);
    strcat(path_des, "/");
    safe_create_dir(path_des);
    strcat(path_des, bname);
    rename(path_src, path_des);
}
```

Untuk mengambil basename maka digunakan fungsi basename

```
char *bname = basename(path_src);
```

Untuk mengambil extension maka digunakan fungsi get_filename_ext

##### Fungsi get_filename_ext dan Fungsi is_hidden

Fungsi ini akan mereturnkan Hidden jika file hidden, lalu akan mereturnkan Unknown jika tidak ditemukan extension pada file. Selain itu, maka akan direturnkan extension yang ada setelah titik

```
void *auto_check(void *args)
{
    ...
    char *ext =strdup(get_filename_ext(bname));
    ...
}

int is_hidden(const char *name)
{
    return name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0;
}

char *get_filename_ext(char *filename)
{
    struct stat st;
    char *dot = strrchr(filename, '.');
    if (is_hidden(filename)) return "Hidden";
    if(!dot || dot == filename) return "Unknown";
    else return dot+1;
}
```

##### Fungsi stringLwr(ext);

Dikarenakan extension akan bersifat case insensitive, maka extension akan dianggap huruf kecil

```
void *auto_check(void *args)
{
    ...
    stringLwr(ext);
    ...
}

void stringLwr(char *s)
{
    int i=0;
    while(s[i]!='\0')
    {
        if(s[i]>='A' && s[i]<='Z')
        {
            s[i]=s[i]+32;
        }
        i++;
    }
}
```

##### Fungsi safe_create_dir

Fungsi ini akan membuat directory jika path yang akan dibuat belum ada, jika sudah ada maka tidak akan dibuat. Lalu, file akan dipindahkan ke dalam directory tersebut

```
void *auto_check(void *args)
{
    ...
    strcpy(path_des, "/home/adindazhr/shift3/hartakarun");
    strcat(path_des, "/");
    strcat(path_des, ext);
    strcat(path_des, "/");
    safe_create_dir(path_des);
    strcat(path_des, bname);
    rename(path_src, path_des);
    ...
}

static void safe_create_dir(const char *dir) {
    if(mkdir(dir,0755) < 0) {
        if (errno != EEXIST) {
            perror(dir);
            exit(1);
        }
    }
}

```

#### Kendala

Dikarenakan belum terlalu mengerti tentang sockets, maka nomor 3.D dan 3.E belum bisa dikerjakan. Lalu karena segmentation fault terjadi jika path adalah sebuah directory dan jika dimasuki lebih dalam lagi maka directory tersebut dihapus.
