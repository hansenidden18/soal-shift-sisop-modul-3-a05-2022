#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

pthread_t thread[3];
pid_t child;
int status;
 
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}

void *unzip(void *args) {
    char *argument = (char *) args;
    char folder[100] = "/home/awan/modul/";
    char zippo[100] = "/home/awan/modul/";
    
    strcat(folder,argument);
    strcat(zippo,argument);

    if ((child = fork()) == 0) {
        execlp("unzip", "unzip", "-q", strcat(zippo,".zip"), "-d", folder, NULL);
    }
    else while ((wait(&status)) > 0);
}

void *decode(void *args){
    FILE *fptr;
    FILE *writeFile;
    char *arg = (char *) args;
    char folder[100] = "/home/awan/modul/";
    char new[100] = "/home/awan/modul/";
    char hasil[100] = "/home/awan/modul/hasil/";

    strcat(hasil, arg);
    strcat(hasil, ".txt");
    strcat(folder, arg);
    strcat(new, arg);
    strcat(new, "/");
    strcat(new, arg);
    strcat(new, ".txt");
    
    struct dirent *dir;
    DIR *file = opendir(folder);
    writeFile = fopen(new, "w");
    fclose(writeFile);

    while((dir = readdir(file)) != NULL){
        if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
            char code[100];
            char answer[100];
            char Files[100] = "/home/awan/modul/";
            strcat(Files, arg);
            strcat(Files, "/");
            strcat(Files, dir->d_name);
            
            fptr = fopen(Files, "r");
            fscanf(fptr, "%s", code);
            long decode_size = strlen(code);
            char *decoded_data = base64_decode(code, decode_size, &decode_size);
            fclose(fptr);

            writeFile = fopen(new, "a");
            fprintf(writeFile, "%s\n", decoded_data);
            fclose(writeFile);
            
        }
    }
    closedir(file);

    if ((child = fork()) == 0) {
        execlp("mkdir", "mkdir", "-p", "/home/awan/modul/hasil", NULL);
    }
    else while ((wait(&status)) > 0);

    if ((child = fork()) == 0) {
        execlp("mv", "mv", "-f", new, hasil, NULL);
    }
    else while ((wait(&status)) > 0);
}

void *small_unzip(void *args){
    if ((child = fork()) == 0) {
        execlp("unzip", "unzip", "-P", "mihinomenestawan", "hasil.zip", NULL);
    }
    else while ((wait(&status)) > 0);
}

void *create_no(void *args){
    FILE *writeFile;
    char new[100] = "/home/awan/modul/no.txt";
    char hasil[100] = "/home/awan/modul/hasil/";

    writeFile = fopen(new, "w");
    fclose(writeFile);
    writeFile = fopen(new, "a");
    fprintf(writeFile, "%s\n", "No");
    fclose(writeFile);

    if ((child = fork()) == 0) {
        execlp("mv", "mv", "-f", new, hasil, NULL);
    }
    else while ((wait(&status)) > 0);
}

int main(){
    int i = 0;
    char *name[2] = {"quote","music"};

    // Quote.zip
    if ((child = fork()) == 0) {
        execlp("wget", "wget", " --no-check-certificate", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "/home/awan/modul/quote.zip", NULL);
    }
    else while ((wait(&status)) > 0);

    // Music.zip
    if ((child = fork()) == 0) {
        execlp("wget", "wget", " --no-check-certificate", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "/home/awan/modul/music.zip", NULL);
    }
    else while ((wait(&status)) > 0);

    while(i < 2){
        int err = pthread_create(&(thread[i]), NULL, &unzip, (void*) name[i]);
        if(err != 0){
            printf("\n Can't create thread : [%s]", strerror(err));
        }
        else{
            printf("\n Create thread success\n");
        }
        i++;
    }
    pthread_join( thread[0], NULL);
    pthread_join( thread[1], NULL);

    i = 0;
    while(i < 2){
        int err = pthread_create(&(thread[i]), NULL, &decode, (void*) name[i]);
        if(err != 0){
            printf("\n Can't create thread : [%s]", strerror(err));
        }
        else{
            printf("\n Create thread success\n");
        }
        i++;
    }
    pthread_join( thread[0], NULL);
    pthread_join( thread[1], NULL);

    if ((child = fork()) == 0) {
        execlp("zip", "zip", "-r", "hasil.zip", "hasil", "--password", "mihinomenestawan", NULL);
    }
    else while ((wait(&status)) > 0);

    int err1 = pthread_create(&(thread[0]), NULL, &small_unzip, NULL);
    if(err1 != 0){
        printf("\n Can't create thread : [%s]", strerror(err1));
    }
    else{
        printf("\n Create thread success\n");
    }

    int err2 = pthread_create(&(thread[1]), NULL, &create_no, NULL);
    if(err2 != 0){
        printf("\n Can't create thread : [%s]", strerror(err2));
    }
    else{
        printf("\n Create thread success\n");
    }
    pthread_join( thread[0], NULL);
    pthread_join( thread[1], NULL);

    if ((child = fork()) == 0) {
        execlp("zip", "zip", "-r", "hasil.zip", "hasil", "--password", "mihinomenestawan", NULL);
    }
    else while ((wait(&status)) > 0);
}